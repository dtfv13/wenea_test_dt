# wenea-test


This repository intends to do what is stated in [this file](PruebaBackend.pdf)

# Develop

## Dev Container

It is advised to use vscode + [dev container](https://code.visualstudio.com/docs/devcontainers/containers#_quick-start-open-an-existing-folder-in-a-container) for development. 

```
 >< Dev Containers: Open Folder in Container
```

**Useful django commands**
```sh
# runserver
python src/django_app/manage.py runserver 0.0.0.0:8000

# makemigrations
python src/django_app/manage.py makemigrations

# migrate
python src/django_app/manage.py migrate 
python src/django_app/manage.py migrate --run-syncdb
```

## Dev deploy using docker-compose

**Launch docker-compose**
```sh
docker-compose up -d --build
```
Test it out at [http://localhost:8000](http://localhost:8000).

**Show logs**
```sh
docker-compose logs -f
```

**Interact with the container**
```sh
docker exec -it wenea-test-web-1 /bin/sh  
```