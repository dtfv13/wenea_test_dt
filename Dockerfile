FROM python:3.9.6-alpine AS base

RUN mkdir -p /wenea
WORKDIR /wenea

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql-dev \
    && pip install psycopg2==2.9.5 \
    && apk del build-deps

RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

FROM base AS dev

RUN apk update && \
    apk add --no-cache bash && \
    apk add git && \
    apk add --no-cache openssh

COPY ./entrypoint.sh /wenea/entrypoint.sh
ENTRYPOINT ["/wenea/entrypoint.sh"]
