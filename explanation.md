# Explicación

Añado este texto para explicar hasta donde he llegado, cuál era mi intención y el porqué de mis decisiones.

En primer lugar, quiero aclarar que mi intención era comenzar por hacer una aplicación sencilla. Donde siguiendo el patrón propuesto por django pudiese tener rápidamente algo funcional. Sin embargo, hacía más de un año y medio que no peleaba con la framework y al no tener ningún proyecto base del que partir, he tardado bastante en configurarme un poco el proyecto.

Aquí ya se pueden detectar algunas de las fallas:
- No tiene entorno de producción
- No existe ningún tipo de pre-commit
- El readme es bastante escaso
- No se utiliza gunicorn o tampoco se ha puesto un reverse proxy para servir la API y tener así un entorno más parecido al real.

A parte de estos problemas de diseño, no existen test unitarios y los de integración no abarcan todos los casos. Además la arquitectura no sigue los principios DDD. Algunos errores a destacar:
- No existe un dominio. Lo más cercano que existe al dominio es el modelo de django.
- Su lógica y sus casos de uso están esparcidos entre el archivo de views.py y serializers.py
- La aplicación es totalmente acoplada a django.


# Implementación de la aplicación siguiendo DDD.

La idea inicial era crear dos ramas: una con la aplicación rasa de django y una aplicación que utilizara DDD donde django fuese un adaptador. El árbol de directorios que tenía pensado quedaría de la siguiente forma:

```
├── src
│   ├── app
│   │   ├── domain
│   │   │   └── models
│   │   │       └── charge_point.py
│   │   ├── services
│   │   │   └── charge_point_service.py
│   │   ├── adapters
│   │   │   └── repository.py 
...
│   ├── django_app
│   │   ├── charge_point
│   │   │   ├── models.py
│   │   │   ├── views.py
│   │   │   └── ...
...
├── tests
│   ├── e2e
│   │   └── ...
│   ├── integration
│   │   └── ...
│   ├── unit
│   │   ├── app
│   │   │   ├── domain
│   │   │   │   └── models
│   │   │   │       └── charge_point.py
│   │   │   ├── services
│   │   │   │   └── charge_point_service.py 
│   │   │   └── ...
```

## src/app

Pseudocódigo `src/app/domain/models/charge_point.py`
```Python
# Quizás un enum...
ChargePointStatus = typing.Literal["ready", "charging", "..."]

# La validación se haría en la misma clase, podría usarse pydantic, la función post_init, etc. Tendría que hacer alguna prueba
@dataclass()
class ChargePoint:
    id: UUID
    name: str
    status: ChargePointStatus
    created_at: Datetime
    deleted_at: Datetime
```

Pseudocódigo `src/app/services/charge_point_service.py`
```Python
from src.app.domain.models.charge_point import ChargePoint

def add_charge_point(charge_point: ChargePoint, repository: AbstractRepository) -> ChargePoint:
    # do something with ChargePoint
    return repository.add(charge_point)

def update_charge_point(charge_point: ChargePoint, repository: AbstractRepository) -> ChargePoint:
    # do something with ChargePoint
    return repository.update(charge_point)

def delete_charge_point(id, repository: AbstractRepository) -> ChargePoint:
    # do something with ChargePoint
    return repository.delete(id)

...

# Para la elección del respositorio se haría mediante inyección de dependencias
```

Pseudocódigo `src/app/adapters/repository.py`
```Python
from src.app.domain.models.charge_point import ChargePoint
from src.django_app.charge_point.models import ChargePoint as DjangoChargePoint

class AbstractRepository(abc.ABC):
    def list(self, id) -> List[ChargePoint]:
        raise NotImplementedError

    def add(self, charge_point: ChargePoint):
        raise NotImplementedError

    def get(self, id) -> ChargePoint:
        raise NotImplementedError

    def update(self, charge_point: ChargePoint) -> ChargePoint:
        raise NotImplementedError

    def delete(self, id) -> ChargePoint:
        raise NotImplementedError


class DjangoRepository(AbstractRepository):
    def add(self, charge_point: ChargePoint):
        charge_point = DjangoChargePoint(charge_point)
        charge_point.save()
        return charge_point.to_domain()

    def update(self, charge_point: ChargePoint):
        return DjangoChargePoint.update_from_domain(charge_point).to_domain()

    def get(self, charge_point: ChargePoint):
        return DjangoChargePoint.objects.filter(deleted_at__isnull=True).to_domain()

    def list(self):
        return [charge_point.to_domain() for charge_point in DjangoChargePoint.objects.all()]
    
    def delete(self, id) -> ChargePoint:
        # TODO
```

## src/django_app

Pseudocódigo `src/django_app/charge_point/models.py`
```Python
class ChargePoint(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=32, unique=True)
    status = models.CharField(
        max_length=8, choices=CHARGE_POINT_STATUS_CHOICES, default="error"
    )
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    deleted_at = models.DateTimeField(blank=True, null=True)

    @staticmethod
    def update_from_domain(charge_point: DomainChargePoint) -> DomainChargePoint:
        try:
            upated_charge_point = ChargePoint.objects.get(reference=charge_point.id)
        except ChargePoint.DoesNotExist:
            upated_charge_point = ChargePoint(charge_point)
        upated_charge_point.id = batch.sku
        upated_charge_point.status = batch._purchased_quantity
        upated_charge_point.save()

    def to_domain(self) -> DomainChargePoint:
        return DomainChargePoint(
            id=self.id, status=self.status, name=self.name, crated_at=self.created_at, deleted_at=self.deleted_at
        )
```

Pseudocódigo `src/django_app/charge_point/views.py`
```Python
# Nótese que la lógica la realiza la capa de servicio
from src.app.services.charge_point_service import update_charge_point

class ChargePointList(APIView):
    """
    List all charge points, or create a new one.
    """

    ...

    def post(self, request, format=None):
        try:
            domain_charge_point = DomainChargePoint(request.data)
            updated_charge_point = update_charge_point(charge_point)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(dict(updated_charge_point), status=status.HTTP_201_CREATED)

```

## tests/unit

Replicando la estructura de la carpeta `src`, crearía test los test unitarios. Tales como:
- test del dominio
- test del servicio
- test del modelo de django (`update_from_domain`, `to_domain`, etc)