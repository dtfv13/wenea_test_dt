from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import ChargePointDetail, ChargePointList

urlpatterns = [
    path("", ChargePointList.as_view(), name="charge_point_list"),
    path("<str:id>/", ChargePointDetail.as_view(), name="charge_point_detail"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
