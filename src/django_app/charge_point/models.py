import uuid

from django.db import models

CHARGE_POINT_STATUS_CHOICES = [
    ("ready", "ready"),
    ("charging", "charging"),
    ("waiting", "waiting"),
    ("error", "error"),
]


class ChargePoint(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=32, unique=True)
    status = models.CharField(
        max_length=8, choices=CHARGE_POINT_STATUS_CHOICES, default="error"
    )
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    deleted_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return str(self.id)
