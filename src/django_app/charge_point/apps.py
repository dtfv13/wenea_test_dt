from django.apps import AppConfig


class ChargepointApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "src.django_app.charge_point"
