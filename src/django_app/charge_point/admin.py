from django.contrib import admin

from .models import ChargePoint

class ChargePointAdmin(admin.ModelAdmin):
    pass
admin.site.register(ChargePoint, ChargePointAdmin)
