from rest_framework import serializers
from .models import ChargePoint


class ChargePointSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChargePoint
        fields = "__all__"
        read_only_fields = ["id", "create_at", "deleted_at"]
