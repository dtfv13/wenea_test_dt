from .models import ChargePoint
from .serializers import ChargePointSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from datetime import datetime


class ChargePointList(APIView):
    """
    List all charge points, or create a new one.
    """

    def get(self, request, format=None):
        charge_points = ChargePoint.objects.filter(deleted_at__isnull=True)
        serializer = ChargePointSerializer(charge_points, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ChargePointSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChargePointDetail(APIView):
    """
    Retrieve, update or delete a charge point instance.
    """

    def get_object(self, id):
        try:
            charge_point = ChargePoint.objects.get(id=id)
            if charge_point.deleted_at is not None:
                raise ReferenceError("ChargePoint was deleted")
            return charge_point
        except ChargePoint.DoesNotExist:
            raise Http404

    def get(self, request, id, format=None):
        charge_point = self.get_object(id)
        serializer = ChargePointSerializer(charge_point)
        return Response(serializer.data)

    def put(self, request, id, format=None):
        charge_point = self.get_object(id)
        serializer = ChargePointSerializer(charge_point, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, format=None):
        charge_point = self.get_object(id)
        charge_point.deleted_at = datetime.now()
        charge_point.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
