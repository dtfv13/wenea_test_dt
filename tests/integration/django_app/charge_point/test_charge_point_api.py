import json
import pytest
from datetime import datetime

from django.urls import reverse_lazy

from src.django_app.charge_point.models import ChargePoint

@pytest.fixture
def charge_point_A() -> ChargePoint:
    return ChargePoint.objects.create(name="A")

@pytest.fixture
def charge_point_deleted_Z() -> ChargePoint:
    return ChargePoint.objects.create(name="Z", deleted_at=datetime.now())


@pytest.mark.django_db(transaction=True)
def test_get_charge_point_list(client, charge_point_deleted_Z, charge_point_A):
    url = reverse_lazy("charge_point_list")
    response = client.get(url)
    assert response.status_code == 200
    assert response.data[0]["name"] == charge_point_A.name


# TODO parametrize: test assertions (status, long name, etc)
@pytest.mark.django_db(transaction=True)
def test_create_new_charge_point(client):
    url = reverse_lazy("charge_point_list")
    data = {"id":123, "name": "new_charge_point", "status":"ready"}
    response = client.post(url, data=json.dumps(data), content_type='application/json')
    assert response.status_code == 201
    assert response.data["name"] == data["name"]


@pytest.mark.django_db(transaction=True)
def test_get_charge_point_by_id(client, charge_point_A):
    url = reverse_lazy("charge_point_detail", kwargs={'id': str(charge_point_A.id)})
    response = client.get(url)
    assert response.status_code == 200
    assert response.data["name"] == charge_point_A.name


# TODO parametrize: test name, id, status, etc
@pytest.mark.django_db(transaction=True)
def test_update_charge_point(client, charge_point_A):
    url = reverse_lazy("charge_point_detail", kwargs={'id': str(charge_point_A.id)})
    data = {"name":"other_name", "status":"ready"}
    response = client.put(url, data=json.dumps(data), content_type='application/json')
    assert response.status_code == 200
    assert response.data["name"] == data["name"]
    assert response.data["status"] == data["status"]
    

@pytest.mark.django_db(transaction=True)
def test_delete_charge_point(client, charge_point_A):
    url = reverse_lazy("charge_point_detail", kwargs={'id': str(charge_point_A.id)})
    response = client.delete(url)
    assert response.status_code == 204
    charge_point = ChargePoint.objects.get(id=str( charge_point_A.id))
    assert (charge_point.deleted_at is not None)